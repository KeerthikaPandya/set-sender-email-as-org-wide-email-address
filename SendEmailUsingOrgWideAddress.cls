public static void sendAuditEmail(String programManagerId , String appId){
        Boolean wasSuccessful;
        String emailTemplateId;
        String emailTemplateName = 'UT_AuditEmail';
        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address =: System.Label.UT_Org_Wide_EmailAddress];
        // Query Email Templates // 
        List<EmailTemplate> emailTemplateQuery = [
            SELECT Id, Name, DeveloperName, IsActive, Description, Subject, HtmlValue, Body 
            FROM EmailTemplate 
            WHERE DeveloperName =:emailTemplateName
            LIMIT 1];
        
        User user = [SELECT Id , Name , Email FROM User WHERE Id =:programManagerId LIMIT 1];
        Application__c appCon = [SELECT Id,Contact__c FROM Application__c WHERE Id=:appId];
        
        if(!emailTemplateQuery.isEmpty()){
            emailTemplateId = emailTemplateQuery[0].Id;
        }
        
        if(emailTemplateId != null && programManagerId !=null){
            
            Messaging.SingleEmailMessage emailMessage = new Messaging.SingleEmailMessage();
            emailMessage.setToAddresses(new List<String>{user.Email});
            emailMessage.setTargetObjectId(appCon.Contact__c);
            emailMessage.setWhatId(appId);
            emailMessage.setTreatTargetObjectAsRecipient(false);
            emailMessage.setReplyTo('noreply@utahdeq.com');
            emailMessage.setSaveAsActivity(false); 
            emailMessage.setTemplateID(emailTemplateId); 
            emailMessage.setOrgWideEmailAddressId(owea.get(0).Id);

            
            Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage> {emailMessage};
                Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
            
            if (results[0].success) {
                wasSuccessful = true;
                
            } else {
                wasSuccessful = false;
                
            }
        }
        else{
            wasSuccessful = false;
        }    
    }
       